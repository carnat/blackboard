<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Summary information';

?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="col-md-6">
    <table class="table table-striped table-bordered detail-view">
        <thead>BlackBord instance information</thead>
        <tbody>
        <?php foreach ($system->learn as $prop => $value) : ?>
            <tr>
                <th><?= $prop ?></th>
                <td><?= $value ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody><tr>
                        <td><span class="totals-head">Total</span></td>
                        <td>
                            <span class="totals-value"><?= count($users); ?> </span>
                            <span class="totals-label">Users</span>
                        </td>
                        <td>
                            <span class="totals-value"><?= count($courses); ?> </span>
                            <span class="totals-label">Courses</span>
                        </td>
                        <td>
                            <span class="totals-value"><?= count($courseRoles); ?> </span>
                            <span class="totals-label">Course Roles</span>
                        </td>
                        <td>
                            <span class="totals-value"><?= count($systemRoles); ?> </span>
                            <span class="totals-label">System Roles</span>
                        </td>
                        <td>
                            <span class="totals-value"><?= count($dataSources); ?> </span>
                            <span class="totals-label">Data Sources</span>
                        </td>
                    </tr>

                    </tbody></table>
            </div>
            <!-- /.box-body -->

        </div>
    </div>
</div>
