<?php

use yii\helpers\Html;

$this->title = 'System Roles';
$this->params['breadcrumbs'][] = ['label' => 'System Roles', 'url' => ['/system-role']];

?>


<div class="language-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-10">
        <table class="table table-striped table-bordered detail-view">
            <thead>
            <tr>
                <th>ID</th>
                <th>Role ID</th>
                <th>Name</th>
                <th>Description</th>
                <th class="action-column">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($systemRoles as $role) : ?>
                <tr>
                    <td style="width:70px;"><?= $role->id ?></td>
                    <td style="width:70px;"><?= $role->roleId ?></td>
                    <td><?= $role->name ?></td>
                    <td><?= $role->description ?></td>
                    <td><a href="/system-role/<?= $role->id ?>" title="View" aria-label="View" data-pjax="0">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>