<?php

use yii\helpers\Html;

$this->title = 'Users';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['/users']];

?>


<div class="language-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-6">
        <table class="table table-striped table-bordered detail-view">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>User Name</th>
                    <th>Gender</th>
                    <th class="action-column">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user) : ?>
                <tr>
                    <td style="width:70px;"><?= $user->id ?></td>
                    <td><?= $user->userName ?></td>
                    <td><?= $user->gender ?></td>
                    <td><a href="/user/<?= $user->id ?>" title="View" aria-label="View" data-pjax="0">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
