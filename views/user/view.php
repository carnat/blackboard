<?php

use yii\helpers\Html;

$this->title = $user->userName;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['/user']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-6">
        <table class="table table-striped table-bordered detail-view">
            <tbody>
                <?php foreach ($user->toArray() as $prop => $value) : ?>
                    <tr>
                        <th><?= $prop ?></th>
                        <td><?= is_array($value) ? key($value) : $value ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>