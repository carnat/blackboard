<?php

use yii\helpers\Html;

$this->title = 'Courses';
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['/course']];

?>


<div class="language-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-6">
        <table class="table table-striped table-bordered detail-view">
            <thead>
            <tr>
                <th>ID</th>
                <th>Course ID</th>
                <th>Name</th>
                <th>Description</th>
                <th class="action-column">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($courses as $course) : ?>
                <tr>
                    <td style="width:70px;"><?= $course->id ?></td>
                    <td style="width:70px;"><?= $course->courseId ?></td>
                    <td><?= $course->name ?></td>
                    <td><?= $course->description ?></td>
                    <td><a href="/course/<?= $course->id ?>" title="View" aria-label="View" data-pjax="0">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>