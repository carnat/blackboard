<?php

use yii\helpers\Html;

$this->title = $course->name;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['/course']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-6">
        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <?php foreach ($course->toArray() as $prop => $value) : ?>
                <tr>
                    <th><?= $prop ?></th>
                    <td><?= is_array($value) ? key($value) : $value ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>