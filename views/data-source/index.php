<?php

use yii\helpers\Html;

$this->title = 'Data Sources';
$this->params['breadcrumbs'][] = ['label' => 'Data Sources', 'url' => ['/data-source']];

?>


<div class="language-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-10">
        <table class="table table-striped table-bordered detail-view">
            <thead>
            <tr>
                <th>ID</th>
                <th>Data Source ID</th>
                <th>Name</th>
                <th>Education Level</th>
                <th class="action-column">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($dataSources as $data) : ?>
                <tr>
                    <td style="width:70px;"><?= $data->id ?></td>
                    <td style="width:70px;"><?= $data->dataSourceId ?></td>
                    <td><?= $data->userName ?></td>
                    <td><?= $data->educationLevel ?></td>
                    <td><a href="/data-source/<?= $data->id ?>" title="View" aria-label="View" data-pjax="0">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>