<?php

use yii\helpers\Html;

$this->title = $courseRole->nameForCourses;
$this->params['breadcrumbs'][] = ['label' => 'Course Roles', 'url' => ['/course-role']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="language-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-6">
        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <?php foreach ($courseRole->toArray() as $prop => $value) : ?>
                <tr>
                    <th><?= $prop ?></th>
                    <td><?= is_array($value) ? key($value) : $value ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>