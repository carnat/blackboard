<?php

use yii\helpers\Html;

$this->title = 'Course Roles';
$this->params['breadcrumbs'][] = ['label' => 'Course Roles', 'url' => ['/course-role']];

?>


<div class="language-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-6">
        <table class="table table-striped table-bordered detail-view">
            <thead>
            <tr>
                <th>ID</th>
                <th>Role ID</th>
                <th>Name For Courses</th>
                <th>Name For Organizations</th>
                <th class="action-column">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($courseRoles as $role) : ?>
                <tr>
                    <td style="width:70px;"><?= $role->id ?></td>
                    <td style="width:70px;"><?= $role->roleId ?></td>
                    <td><?= $role->nameForCourses ?></td>
                    <td><?= $role->nameForOrganizations ?></td>
                    <td><a href="/course-role/<?= $role->id ?>" title="View" aria-label="View" data-pjax="0">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>