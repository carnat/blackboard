<?php

$user = Yii::$app->user->identity;

?>

<aside class="main-sidebar">

    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Dashboard',
                        'icon' => 'link',
                        'url' => '/',
                    ],[
                        'label' => 'Users',
                        'icon' => 'users',
                        'url' => '/user',
                    ],[
                        'label' => 'Courses',
                        'icon' => 'graduation-cap',
                        'url' => '/course',
                    ],[
                        'label' => 'Course Roles',
                        'icon' => 'user-circle',
                        'url' => '/course-role',
                    ],[
                        'label' => 'System Roles',
                        'icon' => 'address-card',
                        'url' => '/system-role',
                    ],[
                        'label' => 'Data Sources',
                        'icon' => 'database',
                        'url' => '/data-source',
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
