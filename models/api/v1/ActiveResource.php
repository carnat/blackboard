<?php

namespace app\models\api\v1;

use Yii;
use yii\base\Arrayable;

class ActiveResource extends Endpoint implements Arrayable
{
    protected $data;

    /**
     * Returns the fully qualified name of this class.
     * @return string the fully qualified name of this class.
     */
    public static function className()
    {
        return get_called_class();
    }

    /**
     * Returns all entities
     * @return array
     */
    public static function find()
    {
        if (!$entities = Yii::$app->cache->get(get_called_class())) {
            $data = static::get();
            $entities = static::populateMultiple($data);
            Yii::$app->cache->set(get_called_class(), $entities, 60 * 3);
        }

        return $entities;
    }

    /**
     * Returns one entity by ID
     * @param string $id
     * @return static
     */
    public static function findOne(string $id)
    {
        $data = static::get($id);
        return static::populate($data);
    }

    public function __get($name)
    {
        $methodName = 'get' . ucfirst($name);
        if (method_exists(get_called_class(), $methodName)) {
            return $this->$methodName();
        } else if (isset($this->data[$name])) {
            return $this->data[$name];
        }

        return null;
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    protected static function populateMultiple($data)
    {
        $collection = [];

        if (empty($data) || !isset($data['results'])) {
            return $collection;
        }

        foreach ($data['results'] as $entity) {
            $collection[] = static::populate($entity);
        }

        return $collection;
    }

    protected static function populate($entity)
    {
        if (empty($entity)) {
            return null;
        }
        $model = get_called_class();

        $object = new $model();
        $object->data = $entity;

        return $object;
    }

    /**
     * Returns the list of fields that should be returned by default by [[toArray()]] when no specific fields are specified.
     *
     * A field is a named element in the returned array by [[toArray()]].
     *
     * This method should return an array of field names or field definitions.
     * If the former, the field name will be treated as an object property name whose value will be used
     * as the field value. If the latter, the array key should be the field name while the array value should be
     * the corresponding field definition which can be either an object property name or a PHP callable
     * returning the corresponding field value. The signature of the callable should be:
     *
     * ```php
     * function ($model, $field) {
     *     // return field value
     * }
     * ```
     *
     * For example, the following code declares four fields:
     *
     * - `email`: the field name is the same as the property name `email`;
     * - `firstName` and `lastName`: the field names are `firstName` and `lastName`, and their
     *   values are obtained from the `first_name` and `last_name` properties;
     * - `fullName`: the field name is `fullName`. Its value is obtained by concatenating `first_name`
     *   and `last_name`.
     *
     * ```php
     * return [
     *     'email',
     *     'firstName' => 'first_name',
     *     'lastName' => 'last_name',
     *     'fullName' => function ($model) {
     *         return $model->first_name . ' ' . $model->last_name;
     *     },
     * ];
     * ```
     *
     * @return array the list of field names or field definitions.
     * @see toArray()
     */
    public function fields()
    {
        // TODO: Implement fields() method.
    }

    /**
     * Returns the list of additional fields that can be returned by [[toArray()]] in addition to those listed in [[fields()]].
     *
     * This method is similar to [[fields()]] except that the list of fields declared
     * by this method are not returned by default by [[toArray()]]. Only when a field in the list
     * is explicitly requested, will it be included in the result of [[toArray()]].
     *
     * @return array the list of expandable field names or field definitions. Please refer
     * to [[fields()]] on the format of the return value.
     * @see toArray()
     * @see fields()
     */
    public function extraFields()
    {
        // TODO: Implement extraFields() method.
    }

    /**
     * Converts the object into an array.
     *
     * @param array $fields the fields that the output array should contain. Fields not specified
     * in [[fields()]] will be ignored. If this parameter is empty, all fields as specified in [[fields()]] will be returned.
     * @param array $expand the additional fields that the output array should contain.
     * Fields not specified in [[extraFields()]] will be ignored. If this parameter is empty, no extra fields
     * will be returned.
     * @param bool $recursive whether to recursively return array representation of embedded objects.
     * @return array the array representation of the object
     */
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        return $this->data;
    }
}
