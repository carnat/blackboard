<?php

namespace app\models\api\v1;

class System extends ActiveResource
{
    public static function endpointName()
    {
        return 'system';
    }
}