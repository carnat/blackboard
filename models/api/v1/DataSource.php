<?php

namespace app\models\api\v1;

class DataSource extends ActiveResource
{
    public static function endpointName()
    {
        return 'users';
    }
}