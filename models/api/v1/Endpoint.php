<?php

namespace app\models\api\v1;

use Yii;

abstract class Endpoint
{
    /**
     * @return string
     */
    public static function endpointName()
    {
        return '';
    }

    /**
     * Api URL from config
     * @return mixed
     */
    protected static function getURL()
    {
        return Yii::$app->params['api.url'];
    }

    /**
     * Send API GET request
     * @param string|null $id
     * @return null
     */
    public static function get(string $id = null)
    {
        $url = static::getURL() .'/'. static::endpointName();

        if (!is_null($id)) {
            $url .= '/' . $id;
        }

        $response = Yii::$app->curl->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->addHeaders([
                'Authorization' => 'Bearer ' . static::getToken()
            ])
            ->setOptions([
                'sslVerifyPeer' => false,
                'sslVerifyHost' => false,
            ])->send();

        return $response->isOk ? $response->data : null;
    }

    /**
     * Returns API access token
     * @return mixed
     * @throws \Exception
     */
    protected static function getToken()
    {
        $response = Yii::$app->curl->createRequest()
            ->setMethod('post')
            ->setUrl(static::getURL() .'/oauth2/token')
            ->setData([
                'grant_type' => 'client_credentials',
            ])
            ->addHeaders([
                'Authorization' =>
                    'Basic '.base64_encode(Yii::$app->params['api.key'].':'.Yii::$app->params['api.secret'])
            ])
            ->setOptions([
                'sslVerifyPeer' => false,
                'sslVerifyHost' => false,
            ])->send();
        if ($response->isOk) {
            $data = $response->data;
            $token = $data['access_token'];
            Yii::$app->cache->set('bbToken', $data['access_token'], $data['expires_in'] - 50);
        } else {
            throw new \Exception('Error while receive API access token');
        }

        return $token;
    }
}
