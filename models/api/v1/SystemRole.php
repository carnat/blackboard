<?php

namespace app\models\api\v1;

class SystemRole extends ActiveResource
{
    public static function endpointName()
    {
        return 'systemRoles';
    }
}