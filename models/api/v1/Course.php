<?php

namespace app\models\api\v1;

class Course extends ActiveResource
{
    public static function endpointName()
    {
        return 'courses';
    }
}