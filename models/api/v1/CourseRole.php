<?php

namespace app\models\api\v1;

class CourseRole extends ActiveResource
{
    public static function endpointName()
    {
        return 'courseRoles';
    }
}