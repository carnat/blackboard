<?php

namespace app\models\api\v1;

class User extends ActiveResource
{
    public static function endpointName()
    {
        return 'users';
    }
}
