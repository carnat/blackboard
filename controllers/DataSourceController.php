<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\api\v1\DataSource;

class DataSourceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', ['dataSources' => DataSource::find()]);
    }

    public function actionView($id)
    {
        return $this->render('view', ['dataSource' => DataSource::findOne($id)]);
    }
}
