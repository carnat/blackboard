<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\api\v1\Course;

class CourseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', ['courses' => Course::find()]);
    }

    public function actionView($id)
    {
        return $this->render('view', ['course' => Course::findOne($id)]);
    }
}
