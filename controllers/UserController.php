<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\api\v1\User;

class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', ['users' => User::find()]);
    }

    public function actionView($id)
    {
        return $this->render('view', ['user' => User::findOne($id)]);
    }
}
