<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\api\v1\CourseRole;

class CourseRoleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', ['courseRoles' => CourseRole::find()]);
    }

    public function actionView($id)
    {
        return $this->render('view', ['courseRole' => CourseRole::findOne($id)]);
    }
}
